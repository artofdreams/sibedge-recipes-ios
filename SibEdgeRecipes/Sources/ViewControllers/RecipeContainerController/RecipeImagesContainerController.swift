//
//  RecipeImagesContainerController.swift
//  SibEdgeRecipes
//
//  Created by Denis Vasilyev on 09.02.2018.
//  Copyright © 2018 DV. All rights reserved.
//

import UIKit

class RecipeImagesContainerController: UIViewController {

    @IBOutlet private weak var pageControl: UIPageControl!

    @IBOutlet private weak var pageContainerView: UIView!

    @IBOutlet private weak var currentPageLabel: UILabel!

    @IBOutlet private weak var pageControlTop: NSLayoutConstraint!

    @IBOutlet private weak var pageControlHeight: NSLayoutConstraint!

    private weak var pageContainer: UIPageViewController!

    private var currentIndex = 0

    private var needsHidePageInfo: Bool { return imageURLs.count == 1 }

    private var usesLabelForPageInfo: Bool { return imageURLs.count > Constant.maxPageCount }

    var imageURLs = [URL]()

}

// MARK: - Overrides

extension RecipeImagesContainerController {

    override func viewDidLoad() {
        super.viewDidLoad()

        setupUI()
    }

    override func updateViewConstraints() {
        super.updateViewConstraints()

        if needsHidePageInfo {
            pageControlTop.constant = 0.0
            pageControlHeight.constant = 0.0
        } else {
            pageControlTop.constant = 4.0
            pageControlHeight.constant = 20.0
        }
    }

}

// MARK: - Private methods

private extension RecipeImagesContainerController {

    func setupUI() {
        pageControl.numberOfPages = imageURLs.count
        pageControl.hidesForSinglePage = true
        pageControl.isHidden = usesLabelForPageInfo
        currentPageLabel.isHidden = !usesLabelForPageInfo
        setupCurrentPageUI()

        let pageTransitionStyle: UIPageViewControllerTransitionStyle = needsHidePageInfo ? .pageCurl : .scroll
        let pageContainer = UIPageViewController(transitionStyle: pageTransitionStyle, navigationOrientation: .horizontal, options: nil)
        pageContainer.dataSource = self
        pageContainer.delegate = self
        addChildViewController(pageContainer, to: pageContainerView)
        self.pageContainer = pageContainer

        setPage(withIndex: currentIndex, direction: .forward, animated: false)
    }

    func page(forIndex index: Int) -> RecipeImageViewController? {
        if imageURLs.count == 0 || index < 0 || index >= imageURLs.count { return nil }

        let page = RecipeImageViewController(nib: R.nib.recipeImageViewController)
        page.pageIndex = index
        page.imageURL = imageURLs[index]

        return page
    }

    func setPage(withIndex index: Int, direction: UIPageViewControllerNavigationDirection, animated: Bool, completion: ((Bool) -> Void)? = nil) {
        guard let page = page(forIndex: index) else { return }
        pageContainer.setViewControllers([page], direction: direction, animated: animated, completion: completion)
    }

    func setupCurrentPageUI() {
        pageControl.currentPage = currentIndex
        currentPageLabel.text = R.string.localizable.recipeCurrentPhotoFormat(currentIndex + 1, imageURLs.count)
    }

}

// MARK: - UIPageViewControllerDataSource

extension RecipeImagesContainerController: UIPageViewControllerDataSource {

    func pageViewController(_ pageViewController: UIPageViewController, viewControllerBefore viewController: UIViewController) -> UIViewController? {
        guard
            let page = viewController as? RecipeImageViewController,
            var index = page.pageIndex,
            index != 0
        else {
            return nil
        }

        index -= 1

        return self.page(forIndex: index)
    }

    func pageViewController(_ pageViewController: UIPageViewController, viewControllerAfter viewController: UIViewController) -> UIViewController? {
        guard
            let page = viewController as? RecipeImageViewController,
            var index = page.pageIndex,
            index != (imageURLs.count - 1)
        else {
            return nil
        }

        index += 1

        return self.page(forIndex: index)
    }

}

// MARK: - UIPageViewControllerDelegate

extension RecipeImagesContainerController: UIPageViewControllerDelegate {

    func pageViewController(_ pageViewController: UIPageViewController, willTransitionTo pendingViewControllers: [UIViewController]) {
        guard let page = pendingViewControllers.first as? RecipeImageViewController, let nextIndex = page.pageIndex else { return }
        currentIndex = nextIndex
    }

    func pageViewController(_ pageViewController: UIPageViewController,
                            didFinishAnimating finished: Bool,
                            previousViewControllers: [UIViewController],
                            transitionCompleted completed: Bool) {

        guard let page = previousViewControllers.first as? RecipeImageViewController, let previousIndex = page.pageIndex else { return }

        if !completed {
            currentIndex = previousIndex
        }

        setupCurrentPageUI()
    }

}

// MARK: - Nested types

extension RecipeImagesContainerController {

    enum Constant {

        static let maxPageCount = 10

    }

}
