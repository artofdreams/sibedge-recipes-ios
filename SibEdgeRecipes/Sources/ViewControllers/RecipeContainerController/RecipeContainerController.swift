//
//  RecipeContainerController.swift
//  SibEdgeRecipes
//
//  Created by Denis Vasilyev on 09.02.2018.
//  Copyright © 2018 DV. All rights reserved.
//

import UIKit

class RecipeContainerController: UIViewController {

    @IBOutlet private weak var imagesContainerView: UIView!

    @IBOutlet private weak var infoContainerView: UIView!

    @IBOutlet private weak var imagesContainerViewHeight: NSLayoutConstraint!

    private var isPhotosPageInfoHidden: Bool {
        guard let recipe = self.recipe else { return true }
        return recipe.imageURLs.count == 1
    }

    var recipe: Recipe?

}

// MARK: - Overrides

extension RecipeContainerController {

    override func viewDidLoad() {
        super.viewDidLoad()

        setupUI()
    }

    override func updateViewConstraints() {
        super.updateViewConstraints()

        imagesContainerViewHeight.constant = isPhotosPageInfoHidden ? 128.0 : 152.0
    }

}

// MARK: - Private methods

private extension RecipeContainerController {

    func setupUI() {
        guard let recipe = self.recipe else { return }

        navigationItem.title = R.string.localizable.recipeInfoNavigationTitle()

        let imagesContainer = RecipeImagesContainerController(nib: R.nib.recipeImagesContainerController)
        imagesContainer.imageURLs = recipe.imageURLs
        addChildViewController(imagesContainer, to: imagesContainerView, verticalConstraintsPriority: UILayoutPriority(rawValue: 500.0))

        let infoViewController = RecipeInfoViewController(nib: R.nib.recipeInfoViewController)
        infoViewController.recipe = recipe
        addChildViewController(infoViewController, to: infoContainerView, verticalConstraintsPriority: UILayoutPriority(rawValue: 500.0))
    }

}
