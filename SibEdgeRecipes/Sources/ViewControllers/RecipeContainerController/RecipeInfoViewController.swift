//
//  RecipeInfoViewController.swift
//  SibEdgeRecipes
//
//  Created by Denis Vasilyev on 09.02.2018.
//  Copyright © 2018 DV. All rights reserved.
//

import UIKit

class RecipeInfoViewController: UIViewController {

    @IBOutlet private weak var difficultyView: RatingView!

    @IBOutlet private weak var nameLabel: UILabel!

    @IBOutlet private weak var descriptionLabel: UILabel!

    @IBOutlet private weak var instructionsLabel: UILabel!

    @IBOutlet private weak var descriptionLabelBottom: NSLayoutConstraint!

    var recipe: Recipe?

}

// MARK: - Overrides

extension RecipeInfoViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        setupUI()
    }

    override func updateViewConstraints() {
        super.updateViewConstraints()

        descriptionLabelBottom.constant = (recipe?.description != nil) ? 16.0 : 0.0
    }

}

// MARK: - Private methods

private extension RecipeInfoViewController {

    func setupUI() {
        guard let recipe = self.recipe else { return }

        nameLabel.text = recipe.name
        descriptionLabel.text = recipe.description
        instructionsLabel.text = recipe.instructions
        difficultyView.rating = recipe.difficulty.rawValue
    }

}
