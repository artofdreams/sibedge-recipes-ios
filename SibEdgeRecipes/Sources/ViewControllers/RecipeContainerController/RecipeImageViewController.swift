//
//  RecipeImageViewController.swift
//  SibEdgeRecipes
//
//  Created by Denis Vasilyev on 09.02.2018.
//  Copyright © 2018 DV. All rights reserved.
//

import UIKit
import AlamofireImage

class RecipeImageViewController: UIViewController {

    @IBOutlet private weak var pictureImageView: UIImageView!

    @IBOutlet private weak var loadingIndicator: UIActivityIndicatorView!

    var imageURL: URL?

    var pageIndex: Int?

}

// MARK: - Overrides

extension RecipeImageViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        setupUI()
    }

}

// MARK: - Private methods

private extension RecipeImageViewController {

    func setupUI() {
        guard let imageURL = self.imageURL else { return }

        loadingIndicator.startAnimating()

        pictureImageView.af_setImage(withURL: imageURL, imageTransition: .crossDissolve(0.25)) { [weak self] (_) in
            self?.loadingIndicator.stopAnimating()
        }
    }

}
