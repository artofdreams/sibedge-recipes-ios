//
//  ItemPickerController.swift
//  SibEdgeRecipes
//
//  Created by Denis Vasilyev on 09.02.2018.
//  Copyright © 2018 DV. All rights reserved.
//

import UIKit

class ItemPickerController: UIViewController {

    @IBOutlet private weak var tableView: UITableView!

    var items = [String]()

    var selectedIndices = [Int]() {
        didSet {
            var indices = selectedIndices

            if indices.count > 1 {
                if let toRemove = indices.index(of: Constant.noSelectionIndex), isMultipleSelectionAllowed {
                    indices.remove(at: toRemove)
                } else {
                    indices = Array(indices.prefix(1))
                }
            }

            if indices.isEmpty {
                indices.append(Constant.noSelectionIndex)
            }

            selectedIndices = indices
        }
    }

    var isMultipleSelectionAllowed = false

    var isNoSelectionAllowed = false

    var selectionChanged: (([Int]) -> Void)?

    private var _selectedIndices = [Int]()

}

// MARK: - Overrides

extension ItemPickerController {

    override func viewDidLoad() {
        super.viewDidLoad()

        _selectedIndices = selectedIndices
        setupUI()
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)

        if let firstSelectedIndex = selectedIndices.first, firstSelectedIndex != Constant.noSelectionIndex {
            tableView.scrollToRow(at: IndexPath(row: firstSelectedIndex, section: 0), at: .middle, animated: true)
        }
    }

}

// MARK: - Private methods

private extension ItemPickerController {

    func setupUI() {
        let preferredHeight = Constant.rowHeight * CGFloat(((items.count > Constant.visibleItemsCount) ? Constant.visibleItemsCount : items.count))
        preferredContentSize = CGSize(width: Constant.preferredWidth, height: preferredHeight)

        tableView.register(R.nib.itemPickerCell)
        tableView.scrollsToTop = false
        tableView.rowHeight = Constant.rowHeight
    }

}

// MARK: - UITableViewDataSource

extension ItemPickerController: UITableViewDataSource {

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return items.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let identifier = R.reuseIdentifier.itemPickerCell
        guard let cell = tableView.dequeueReusableCell(withIdentifier: identifier, for: indexPath) else { return UITableViewCell() }

        cell.titleLabel.text = items[indexPath.row]
        cell.checkmarkImageView.isHidden = !_selectedIndices.contains(indexPath.row)
        cell.separatorView.isHidden = (indexPath.row == (items.count - 1))

        return cell
    }

}

// MARK: - UITableViewDelegate

extension ItemPickerController: UITableViewDelegate {

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)

        tableView.beginUpdates()

        let selectedIndex = indexPath.row

        if let toRemove = _selectedIndices.index(of: selectedIndex) {
            _selectedIndices.remove(at: toRemove)

            if !isNoSelectionAllowed {
                let count = _selectedIndices.count

                if count == 0 {
                    _selectedIndices.append(selectedIndex)
                } else if (count == 1) && (_selectedIndices.first == Constant.noSelectionIndex) {
                    _selectedIndices[0] = selectedIndex
                }
            }
        } else {
            if !isMultipleSelectionAllowed {
                _selectedIndices.forEach { tableView.reloadRows(at: [IndexPath(row: $0, section: indexPath.section)], with: .fade) }
                _selectedIndices.removeAll()
            }

            _selectedIndices.append(selectedIndex)
        }

        tableView.reloadRows(at: [indexPath], with: .fade)
        tableView.endUpdates()

        selectedIndices = _selectedIndices
        selectionChanged?(selectedIndices)
    }

}

// MARK: - Nested types

extension ItemPickerController {

    enum Constant {

        static let noSelectionIndex = -1

        static let visibleItemsCount = 5

        static let preferredWidth: CGFloat = 300.0

        static let rowHeight: CGFloat = 40.0

    }

}
