//
//  RecipesSearcher.swift
//  SibEdgeRecipes
//
//  Created by Denis Vasilyev on 08.02.2018.
//  Copyright © 2018 DV. All rights reserved.
//

import Foundation

protocol RecipesSearcherDelegate: class {

    func searcher(_ searcher: RecipesSearcher, didFinishSearchWith filtered: [Recipe])

}

class RecipesSearcher {

    weak var delegate: RecipesSearcherDelegate?

    var source = [Recipe]()

    var searchText = "" {
        didSet {
            guard oldValue != searchText else { return }
            performSearch()
        }
    }

    private(set) var recipes = [Recipe]()

    private let queue: OperationQueue = {
        let queue = OperationQueue()
        queue.name = "recipes.searcher.queue"
        queue.maxConcurrentOperationCount = 1
        return queue
    }()

    private lazy var searchingPredicate: (Recipe) -> Bool = { [unowned self] (recipe) in
        let name = recipe.name.lowercased()
        let instructions = recipe.instructions.lowercased()
        let pattern = self.searchText.lowercased()
        var isIncluded = name.contains(pattern) || instructions.contains(pattern)

        if let description = recipe.description?.lowercased() {
            isIncluded = isIncluded || description.contains(pattern)
        }

        return isIncluded
    }

}

// MARK: - Private methods

private extension RecipesSearcher {

    func performSearch() {
        queue.cancelAllOperations()
        queue.addOperation(searchingOperation())
    }

    func searchingOperation() -> Operation {
        let operation = BlockOperation()

        operation.addExecutionBlock { [unowned operation, weak self] in
            guard let strongSelf = self else { return }

            let filtered = strongSelf.source.filter(strongSelf.searchingPredicate)

            if !operation.isCancelled {
                strongSelf.callDidFinishSearch(with: filtered)
            }
        }

        return operation
    }

    func callDidFinishSearch(with filtered: [Recipe]) {
        DispatchQueue.main.async { [weak self] in
            guard let strongSelf = self else { return }
            strongSelf.recipes = filtered
            strongSelf.delegate?.searcher(strongSelf, didFinishSearchWith: filtered)
        }
    }

}
