//
//  RecipesSorter.swift
//  SibEdgeRecipes
//
//  Created by Denis Vasilyev on 08.02.2018.
//  Copyright © 2018 DV. All rights reserved.
//

import Foundation

protocol RecipesSorterDelegate: class {

    func sorter(_ sorter: RecipesSorter, didSortRecipes sorted: [Recipe])

}

class RecipesSorter {

    weak var delegate: RecipesSorterDelegate?

    var type = RecipesSortingType.name {
        didSet {
            guard oldValue != type else { return }
            update(with: recipes)
        }
    }

    private(set) var recipes = [Recipe]()

    private let queue: OperationQueue = {
        let queue = OperationQueue()
        queue.name = "recipes.sorter.queue"
        queue.maxConcurrentOperationCount = 1
        return queue
    }()

}

// MARK: - Internal methods

extension RecipesSorter {

    func update(with unsorted: [Recipe]) {
        queue.cancelAllOperations()
        queue.addOperation(sortingOperation(with: unsorted))
    }

}

// MARK: - Private methods

private extension RecipesSorter {

    func sortingOperation(with unsorted: [Recipe]) -> Operation {
        let operation = BlockOperation()

        operation.addExecutionBlock { [unowned operation, weak self] in
            guard let strongSelf = self else { return }

            let sorted = unsorted.sorted(by: strongSelf.type.predicate)

            if !operation.isCancelled {
                strongSelf.callDidSortRecipes(sorted)
            }
        }

        return operation
    }

    func callDidSortRecipes(_ sorted: [Recipe]) {
        DispatchQueue.main.async { [weak self] in
            guard let strongSelf = self else { return }
            strongSelf.recipes = sorted
            strongSelf.delegate?.sorter(strongSelf, didSortRecipes: sorted)
        }
    }

}
