//
//  RecipesSearchResultsController.swift
//  SibEdgeRecipes
//
//  Created by Denis Vasilyev on 08.02.2018.
//  Copyright © 2018 DV. All rights reserved.
//

import UIKit

class RecipesSearchResultsController: UIViewController {

    @IBOutlet private weak var tableView: UITableView!

    var recipeSelected: ((Recipe) -> Void)?

    private let searcher = RecipesSearcher()

}

// MARK: - Overrides

extension RecipesSearchResultsController {

    override func viewDidLoad() {
        super.viewDidLoad()

        searcher.delegate = self
        setupUI()
    }

}

// MARK: - Internal methods

extension RecipesSearchResultsController {

    func updateSource(_ source: [Recipe]) {
        searcher.source = source
    }

}

// MARK: - Private methods

private extension RecipesSearchResultsController {

    func setupUI() {
        tableView.register(R.nib.recipeCell)
        tableView.rowHeight = UITableViewAutomaticDimension
        tableView.estimatedRowHeight = 65.0
        tableView.tableFooterView = UIView()
    }

}

// MARK: - UISearchResultsUpdating

extension RecipesSearchResultsController: UISearchResultsUpdating {

    func updateSearchResults(for searchController: UISearchController) {
        searcher.searchText = searchController.searchBar.text ?? ""
    }

}

// MARK: - RecipesSearcherDelegate

extension RecipesSearchResultsController: RecipesSearcherDelegate {

    func searcher(_ searcher: RecipesSearcher, didFinishSearchWith filtered: [Recipe]) {
        tableView.reloadData()
    }

}

// MARK: - UITableViewDataSource

extension RecipesSearchResultsController: UITableViewDataSource {

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return searcher.recipes.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let identifier = R.reuseIdentifier.recipeCell
        guard let cell = tableView.dequeueReusableCell(withIdentifier: identifier, for: indexPath) else { return UITableViewCell() }
        cell.recipe = searcher.recipes[indexPath.row]
        return cell
    }

}

// MARK: - UITableViewDelegate

extension RecipesSearchResultsController: UITableViewDelegate {

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        recipeSelected?(searcher.recipes[indexPath.row])
    }

}
