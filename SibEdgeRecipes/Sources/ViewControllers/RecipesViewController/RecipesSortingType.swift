//
//  RecipesSortingType.swift
//  SibEdgeRecipes
//
//  Created by Denis Vasilyev on 08.02.2018.
//  Copyright © 2018 DV. All rights reserved.
//

import Foundation

enum RecipesSortingType: Int {

    case name
    case lastUpdate

    static var descriptions: [String] {
        return R.string.localizable.recipesSortingTypes().components(separatedBy: ",")
    }

    var predicate: (Recipe, Recipe) -> Bool {
        switch self {
        case .name:
            return { $0.name < $1.name }
        case .lastUpdate:
            return { $0.lastUpdate > $1.lastUpdate }
        }
    }

}
