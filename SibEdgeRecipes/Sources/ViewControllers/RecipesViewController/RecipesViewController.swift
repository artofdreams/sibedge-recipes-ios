//
//  RecipesViewController.swift
//  SibEdgeRecipes
//
//  Created by Denis Vasilyev on 07.02.2018.
//  Copyright © 2018 DV. All rights reserved.
//

import UIKit

class RecipesViewController: UIViewController {

    @IBOutlet private weak var tableView: UITableView!

    @IBOutlet private weak var loadingIndicator: UIActivityIndicatorView!

    private weak var searchResultController: RecipesSearchResultsController?

    private var popoverPresenter: PopoverPresenter!

    private let network = NetworkService()

    private let sorter = RecipesSorter()

}

// MARK: - Overrides

extension RecipesViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        sorter.delegate = self
        setupUI()
        getRecipesFromServer()
    }

}

// MARK: - Actions

extension RecipesViewController {

    @objc func actionChangeSortingType(_ sender: UIBarButtonItem) {
        let picker = ItemPickerController(nib: R.nib.itemPickerController)

        picker.items = RecipesSortingType.descriptions
        picker.selectedIndices = [sorter.type.rawValue]

        picker.selectionChanged = { [weak self] (indices) in
            guard let rawType = indices.first, let type = RecipesSortingType(rawValue: rawType) else { return }
            self?.sorter.type = type
        }

        popoverPresenter.present(for: picker, from: sender)
    }

}

// MARK: - Private methods

private extension RecipesViewController {

    func setupUI() {
        popoverPresenter = PopoverPresenter(sourceViewController: self, sourceView: view)

        tableView.register(R.nib.recipeCell)
        tableView.rowHeight = UITableViewAutomaticDimension
        tableView.estimatedRowHeight = 65.0
        tableView.tableFooterView = UIView()

        let rightItem = UIBarButtonItem(image: R.image.iconSortingType(), style: .plain, target: self, action: #selector(actionChangeSortingType(_:)))
        navigationItem.rightBarButtonItem = rightItem

        let searchResultsController = RecipesSearchResultsController(nib: R.nib.recipesSearchResultController)
        searchResultsController.recipeSelected = { [weak self] in self?.showDetails(for: $0) }
        let searchController = UISearchController(searchResultsController: searchResultsController)
        searchController.searchResultsUpdater = searchResultsController
        self.searchResultController = searchResultsController

        navigationItem.title = R.string.localizable.recipesNavigationTitle()
        navigationItem.hidesSearchBarWhenScrolling = false
        navigationItem.searchController = searchController
        definesPresentationContext = true

        loadingIndicator.startAnimating()
    }

    func getRecipesFromServer() {
        network.getRecipes { [weak self] result in
            self?.loadingIndicator.stopAnimating()

            switch result {
            case .success(let recipes):
                self?.sorter.update(with: recipes)
            case .failure(let error):
                self?.showAlert(title: R.string.localizable.alertTitleError(), message: error.localizedDescription)
            }
        }
    }

    func showDetails(for recipe: Recipe) {
        let recipeController = RecipeContainerController(nib: R.nib.recipeContainerController)
        recipeController.recipe = recipe

        navigationController?.pushViewController(recipeController, animated: true)
    }

}

// MARK: - RecipesSorterDelegate

extension RecipesViewController: RecipesSorterDelegate {

    func sorter(_ sorter: RecipesSorter, didSortRecipes sorted: [Recipe]) {
        searchResultController?.updateSource(sorted)
        tableView.reloadData()
    }

}

// MARK: - UITableViewDataSource

extension RecipesViewController: UITableViewDataSource {

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return sorter.recipes.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let identifier = R.reuseIdentifier.recipeCell
        guard let cell = tableView.dequeueReusableCell(withIdentifier: identifier, for: indexPath) else { return UITableViewCell() }
        cell.recipe = sorter.recipes[indexPath.row]
        return cell
    }

}

// MARK: - UITableViewDelegate

extension RecipesViewController: UITableViewDelegate {

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        showDetails(for: sorter.recipes[indexPath.row])
    }

}
