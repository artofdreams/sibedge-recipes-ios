//
//  Recipe.swift
//  SibEdgeRecipes
//
//  Created by Denis Vasilyev on 07.02.2018.
//  Copyright © 2018 DV. All rights reserved.
//

import ObjectMapper

class Recipe: Mappable {

    private(set) var identifier = UUID()

    private(set) var name = String()

    private(set) var instructions = String()

    private(set) var imageURLs = [URL]()

    private(set) var difficulty = Difficulty.veryEasy

    private(set) var lastUpdate = Date()

    private(set) var description: String?

    required init?(map: Map) {
        if !isValidMap(map) { return nil }
    }

    func mapping(map: Map) {
        identifier <- (map[JSONKey.identifier], Transform.uuid)
        name <- (map[JSONKey.name], Transform.text)
        instructions <- (map[JSONKey.instructions], Transform.text)
        imageURLs <- (map[JSONKey.imageURLs], Transform.urls)
        difficulty <- (map[JSONKey.difficulty], Transform.recipeDifficulty)
        lastUpdate <- (map[JSONKey.lastUpdate], Transform.unixTimestamp)
        description <- (map[JSONKey.description], Transform.text)
    }

}

// MARK: - Nested types

extension Recipe {

    enum Difficulty: Int {

        case veryEasy = 1
        case easy
        case normal
        case hard
        case veryHard

        static let min = Difficulty.veryEasy.rawValue
        static let max = Difficulty.veryHard.rawValue

    }

    enum JSONKey {

        static let identifier = "uuid"
        static let name = "name"
        static let description = "description"
        static let instructions = "instructions"
        static let difficulty = "difficulty"
        static let imageURLs = "images"
        static let lastUpdate = "lastUpdated"

    }

}

// MARK: - Private methods

private extension Recipe {

    func isValidMap(_ map: Map) -> Bool {
        guard
            let uuidString = map.JSON[JSONKey.identifier] as? String, !uuidString.isEmpty,
            let name = map.JSON[JSONKey.name] as? String, !name.isEmpty,
            let instructions = map.JSON[JSONKey.instructions] as? String, !instructions.isEmpty,
            let imageURLStrings = map.JSON[JSONKey.imageURLs] as? [String], !imageURLStrings.isEmpty,
            let rawDifficulty = map.JSON[JSONKey.difficulty] as? Int, Difficulty.min ... Difficulty.max ~= rawDifficulty,
            let lastUpdateTimestamt = map.JSON[JSONKey.lastUpdate] as? Double, lastUpdateTimestamt > 0.0
        else {
            return false
        }

        return true
    }

}
