//
//  NSLayoutConstraint+Additions.swift
//  SibEdgeRecipes
//
//  Created by Denis Vasilyev on 09.02.2018.
//  Copyright © 2018 DV. All rights reserved.
//

import UIKit

extension NSLayoutConstraint {

    @IBInspectable var preciseConstant: CGFloat {
        set {
            constant = newValue / UIScreen.main.scale
        }
        get {
            return constant * UIScreen.main.scale
        }
    }

}
