//
//  UIViewController+Additions.swift
//  SibEdgeRecipes
//
//  Created by Denis Vasilyev on 09.02.2018.
//  Copyright © 2018 DV. All rights reserved.
//

import UIKit

extension UIViewController {

    func addChildViewController(_ child: UIViewController,
                                to container: UIView,
                                horizontalConstraintsPriority: UILayoutPriority? = nil,
                                verticalConstraintsPriority: UILayoutPriority? = nil) {

        addChildViewController(child)
        container.addSubview(child.view)
        child.didMove(toParentViewController: self)

        let horizontalConstraints = NSLayoutConstraint.constraints(withVisualFormat: "H:|[childView]|",
                                                                   options: [],
                                                                   metrics: nil,
                                                                   views: ["childView": child.view])

        let verticalConstraints = NSLayoutConstraint.constraints(withVisualFormat: "V:|[childView]|",
                                                                 options: [],
                                                                 metrics: nil,
                                                                 views: ["childView": child.view])

        if let priority = horizontalConstraintsPriority {
            horizontalConstraints.forEach { $0.priority = priority }
        }

        if let priority = verticalConstraintsPriority {
            verticalConstraints.forEach { $0.priority = priority }
        }

        view.addConstraints(horizontalConstraints + verticalConstraints)

        child.view.translatesAutoresizingMaskIntoConstraints = false
    }

}
