//
//  UIViewController+Alerts.swift
//  SibEdgeRecipes
//
//  Created by Denis Vasilyev on 09.02.2018.
//  Copyright © 2018 DV. All rights reserved.
//

import UIKit

extension UIViewController {

    /* Shows default OK action if actions is nil */
    func showAlert(title: String?, message: String?, actions: [UIAlertAction]? = nil) {
        let alert = UIAlertController(title: title ?? "", message: message, preferredStyle: .alert)

        var alertActions = actions ?? []

        if alertActions.isEmpty {
            alertActions.append(UIAlertAction(title: R.string.localizable.alertActionOk(), style: .default))
        }

        alertActions.forEach { alert.addAction($0) }

        present(alert, animated: true)
    }

}
