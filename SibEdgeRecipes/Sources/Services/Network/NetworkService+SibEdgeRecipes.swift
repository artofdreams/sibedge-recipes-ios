//
//  NetworkService+SibEdgeRecipes.swift
//  SibEdgeRecipes
//
//  Created by Denis Vasilyev on 07.02.2018.
//  Copyright © 2018 DV. All rights reserved.
//

import Alamofire

extension NetworkService {

    func getRecipes(completion: @escaping (Result<[Recipe]>) -> Void) {
        let request = HTTPRequest(resource: .recipes, mappingKeyPath: "recipes")
        makeArrayRequest(request, completion: completion)
    }

}
