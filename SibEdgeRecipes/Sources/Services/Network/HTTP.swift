//
//  HTTP.swift
//  SibEdgeRecipes
//
//  Created by Denis Vasilyev on 07.02.2018.
//  Copyright © 2018 DV. All rights reserved.
//

import Alamofire

struct HTTPRequest {

    let resource: HTTPResource

    let method: HTTPMethod

    let headers: HTTPHeaders

    let parameters: Parameters

    let mappingKeyPath: String?

    init(resource: HTTPResource,
         method: HTTPMethod = .get,
         headers: HTTPHeaders = [:],
         parameters: Parameters = [:],
         mappingKeyPath: String? = nil) {

        self.resource = resource
        self.method = method
        self.headers = headers
        self.parameters = parameters
        self.mappingKeyPath = mappingKeyPath
    }

}

enum HTTPResource: URLConvertible {

    private static let baseURLString = "http://cdn.sibedge.com/temp"

    case recipes

    func asURL() throws -> URL {
        guard let baseURL = URL(string: HTTPResource.baseURLString) else {
            throw AFError.invalidURL(url: HTTPResource.baseURLString)
        }

        switch self {
        case .recipes:
            return baseURL.appendingPathComponent("recipes.json")
        }
    }

}
