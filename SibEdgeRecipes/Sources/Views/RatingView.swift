//
//  RatingView.swift
//  SibEdgeRecipes
//
//  Created by Denis Vasilyev on 09.02.2018.
//  Copyright © 2018 DV. All rights reserved.
//

import UIKit

class RatingView: UIView {

    @IBOutlet private var stars: [UIImageView]!

    /// Count of filled stars.
    var rating = 0 {
        didSet {
            setupRating()
        }
    }

    var fillColor = UIColor(red: 242.0/255.0, green: 220.0/255.0, blue: 9.0/255.0, alpha: 1.0) {
        didSet {
            setupFillColor()
        }
    }

}

// MARK: - Overrides

extension RatingView {

    override func awakeFromNib() {
        super.awakeFromNib()

        stars = stars.sorted { $0.tag < $1.tag }
        setupRating()
        setupFillColor()
    }

}

// MARK: - Private methods

private extension RatingView {

    func setupRating() {
        for (index, star) in stars.enumerated() {
            star.image = (rating > index) ? R.image.iconStarFilled() : R.image.iconStar()
        }
    }

    func setupFillColor() {
        let tintColor = fillColor
        stars.forEach { $0.tintColor = tintColor }
    }

}
