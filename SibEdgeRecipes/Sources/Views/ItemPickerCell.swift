//
//  ItemPickerCell.swift
//  SibEdgeRecipes
//
//  Created by Denis Vasilyev on 09.02.2018.
//  Copyright © 2018 DV. All rights reserved.
//

import UIKit

class ItemPickerCell: UITableViewCell {

    @IBOutlet weak var titleLabel: UILabel!

    @IBOutlet weak var separatorView: UIView!

    @IBOutlet weak var checkmarkImageView: UIImageView!

}
