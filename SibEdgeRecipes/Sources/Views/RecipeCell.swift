//
//  RecipeCell.swift
//  SibEdgeRecipes
//
//  Created by Denis Vasilyev on 08.02.2018.
//  Copyright © 2018 DV. All rights reserved.
//

import UIKit
import AlamofireImage

class RecipeCell: UITableViewCell {

    @IBOutlet private weak var nameLabel: UILabel!

    @IBOutlet private weak var descriptionLabel: UILabel!

    @IBOutlet private weak var pictureImageView: UIImageView!

    var recipe: Recipe? {
        didSet {
            setupUI()
        }
    }

}

// MARK: - Private methods

private extension RecipeCell {

    func setupUI() {
        guard let recipe = recipe else { return }

        nameLabel.text = recipe.name
        descriptionLabel.text = recipe.description

        if let pictureURL = recipe.imageURLs.first {
            pictureImageView.af_setImage(withURL: pictureURL)
        }
    }

}
