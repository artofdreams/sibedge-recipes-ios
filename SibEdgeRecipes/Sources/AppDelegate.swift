//
//  AppDelegate.swift
//  SibEdgeRecipes
//
//  Created by Denis Vasilyev on 07.02.2018.
//  Copyright © 2018 DV. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        loadMainUI()
        return true
    }

}

// MARK: - Private methods

private extension AppDelegate {

    func loadMainUI() {
        let window = UIWindow(frame: UIScreen.main.bounds)
        self.window = window

        let recipesViewController = RecipesViewController(nib: R.nib.recipesViewController)
        window.rootViewController = UINavigationController(rootViewController: recipesViewController)
        window.makeKeyAndVisible()
    }

}
