//
//  PopoverPresenter.swift
//  SibEdgeRecipes
//
//  Created by Denis Vasilyev on 09.02.2018.
//  Copyright © 2018 DV. All rights reserved.
//

import UIKit

class PopoverPresenter: NSObject {

    private weak var sourceViewController: UIViewController!

    private weak var sourceView: UIView!

    private weak var targetView: UIView?

    init(sourceViewController: UIViewController, sourceView: UIView) {
        self.sourceViewController = sourceViewController
        self.sourceView = sourceView
    }

}

// MARK: - Internal methods

extension PopoverPresenter {

    func present(for viewController: UIViewController, from view: UIView) {
        viewController.modalPresentationStyle = .popover

        guard let popoverPresentationController = viewController.popoverPresentationController else { return }

        popoverPresentationController.delegate = self
        popoverPresentationController.permittedArrowDirections = [.up, .down]
        popoverPresentationController.sourceView = sourceView
        popoverPresentationController.sourceRect = sourceView.convert(view.bounds, from: view)
        popoverPresentationController.backgroundColor = viewController.view.backgroundColor

        targetView = view

        sourceViewController.present(viewController, animated: true, completion: nil)
    }

    func present(for viewController: UIViewController, from item: UIBarButtonItem) {
        viewController.modalPresentationStyle = .popover

        guard let popoverPresentationController = viewController.popoverPresentationController else { return }

        popoverPresentationController.delegate = self
        popoverPresentationController.permittedArrowDirections = [.up, .down]
        popoverPresentationController.barButtonItem = item
        popoverPresentationController.backgroundColor = viewController.view.backgroundColor

        sourceViewController.present(viewController, animated: true, completion: nil)
    }

}

// MARK: - UIPopoverPresentationControllerDelegate

extension PopoverPresenter: UIPopoverPresentationControllerDelegate {

    func popoverPresentationControllerShouldDismissPopover(_ popoverPresentationController: UIPopoverPresentationController) -> Bool {
        return true
    }

    func popoverPresentationControllerDidDismissPopover(_ popoverPresentationController: UIPopoverPresentationController) {
        targetView = nil
    }

    func popoverPresentationController(_ popoverPresentationController: UIPopoverPresentationController,
                                       willRepositionPopoverTo rect: UnsafeMutablePointer<CGRect>,
                                       in view: AutoreleasingUnsafeMutablePointer<UIView>) {

        guard let targetView = self.targetView else { return }

        let targetRect = sourceView.convert(targetView.bounds, from: targetView)

        if sourceView.bounds.intersects(targetRect) {
            rect.pointee = targetRect
            view.pointee = sourceView
        } else if sourceViewController.presentedViewController?.modalPresentationStyle == .popover {
            sourceViewController.dismiss(animated: false, completion: nil)
        }
    }

}

// MARK: - UIAdaptivePresentationControllerDelegate

extension PopoverPresenter: UIAdaptivePresentationControllerDelegate {

    func adaptivePresentationStyle(for controller: UIPresentationController) -> UIModalPresentationStyle {
        return .none
    }

    func adaptivePresentationStyle(for controller: UIPresentationController, traitCollection: UITraitCollection) -> UIModalPresentationStyle {
        return .none
    }

}
