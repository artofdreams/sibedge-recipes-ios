//
//  Transform.swift
//  SibEdgeRecipes
//
//  Created by Denis Vasilyev on 08.02.2018.
//  Copyright © 2018 DV. All rights reserved.
//

import ObjectMapper

enum Transform {

    static var uuid: TransformOf<UUID, String> = {
        return TransformOf(fromJSON: { (value) in
            guard let uuidString = value else { return nil }
            return UUID(uuidString: uuidString)
        }, toJSON: { (value) in
            return value?.uuidString
        })
    }()

    static var text: TransformOf<String, String> = {
        return TransformOf(fromJSON: { (value) in
            guard let rawString = value, !rawString.isEmpty else { return nil }
            let condensedWhitespace = rawString.components(separatedBy: .whitespacesAndNewlines).filter { !$0.isEmpty }.joined(separator: " ")
            let separatedByNewlines = condensedWhitespace.components(separatedBy: "<br>").map { $0.trimmingCharacters(in: .whitespaces) }
            return separatedByNewlines.joined(separator: "\n")
        }, toJSON: { (value) in
            return value
        })
    }()

    static var urls: TransformOf<[URL], [String]> = {
        return TransformOf(fromJSON: { (value) in
            guard let urlStrings = value else { return nil }
            return urlStrings.flatMap { URL(string: $0) }
        }, toJSON: { (value) in
            guard let urls = value else { return nil }
            return urls.map { $0.absoluteString }
        })
    }()

    static var recipeDifficulty: TransformOf<Recipe.Difficulty, Int> = {
        return TransformOf(fromJSON: { (value) in
            guard let rawDifficulty = value else { return nil }
            return Recipe.Difficulty(rawValue: rawDifficulty)
        }, toJSON: { (value) in
            return value?.rawValue
        })
    }()

    static var unixTimestamp: TransformOf<Date, Any> = {
        return TransformOf(fromJSON: { (value) in
            if let timestamp = value as? Double {
                return Date(timeIntervalSince1970: timestamp)
            }

            if let timestampString = value as? String, let timestamp = Double(timestampString) {
                return Date(timeIntervalSince1970: timestamp)
            }

            return nil
        }, toJSON: { (value) in
            return value?.timeIntervalSince1970
        })
    }()

}
