SibEdgeRecipes
----------------

Проект использует CocoaPods для управления зависимостями.

Если CocoaPods не установлен, то открываем терминал и ставим его вот так:

	sudo gem install cocoapods
	pod setup

После установки CocoaPods там же, в терминале:

	cd "path to project root directory"
	pod install

После этого открываем воркспейс и собираем проект.